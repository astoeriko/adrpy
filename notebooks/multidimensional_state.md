---
jupyter:
  jupytext:
    formats: ipynb,md
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.0
---

# Simple tracer example
- constant velocity,
- constant concentrations at the boundaries
- 1-dimensonal state variables

```python
# Import all necessary libraries
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sunode
import xarray as xr

import adrpy
import adrpy.velocity_funcs as velo
```

```python
# Define the reactions
def reaction(t, y, p):
    return {
        "z": -p.a * y.z,
    }
```

```python
# Define parameter values
state_shapes = {"z": ("chemical")}
reaction_param_dims = {
    "a": ("chemical"),
    "c_norm": {"z": ("chemical")},
    "spatial_coordinate": (),
}
transport_param_dims = {
    "dx": (),  # length of each cell
    "length": (),  # length of the total domain
    "transported": {"z": ("chemical")},  # indicate if the state is subject to transport
    "v": (),  # constant veloctiy
    "dispersivity": (),
    "molecular_diffusion": (),
    "porosity": (),
    "left": {"inflow_concentration": {"z": ("chemical")}},  # left inflow concentrations
    "right": {"inflow_concentration": {"z": ("chemical")}},
}
```

```python
# Define values of the x-coordinate
dx = 0.01
L = 2.0
x = np.arange(dx / 2, L, dx)
coords = {"x": x, "chemical": ["atrazine", "glyphosate"]}
```

```python
inflow_concentrations = pd.Series(
    {"atrazine": 0.1, "glyphosate": 1.0}, index=coords["chemical"]
)
transport_param_values = {
    "dx": dx,  # length of each cell
    "length": L,  # length of the total domain
    "transported": {
        "z": np.ones_like(inflow_concentrations),
    },  # indicate if the state is subject to transport
    "v": 3e-5,  # constant veloctiy
    "dispersivity": 1e-2,
    "molecular_diffusion": 1e-9,
    "porosity": 0.3,
    "left": {
        "inflow_concentration": {"z": inflow_concentrations.values}
    },  # left inflow concentrations
    "right": {"inflow_concentration": {"z": np.zeros_like(inflow_concentrations)}},
}
decay_constants = pd.Series(
    {"atrazine": 1e-5, "glyphosate": 1e-4}, index=coords["chemical"]
)
reaction_param_values = {
    "a": decay_constants.values,  # decay constant
    # "c_norm" and "spatial_coordinate" are required, even if they are not used
    "c_norm": {"z": np.ones_like(inflow_concentrations)},
    "spatial_coordinate": -333.0,
}
```

```python
# Set up the reaction problem
reaction_problem = sunode.SympyProblem(
    params=reaction_param_dims,
    states=state_shapes,
    rhs_sympy=reaction,
    derivative_params=(),
    coords=coords
)
```

```python
# Set up the transport problem
transport_problem = adrpy.TransportProblem(
    reaction_problem=reaction_problem,
    left_boundary="constant_concentration",
    right_boundary="constant_concentration",
    velocity_func=velo.constant_velocity,
    transport_param_dims=transport_param_dims,
    coords=coords,
)
```

```python
# Generate the solver
solver = sunode.solver.Solver(
    transport_problem,
    solver="BDF",
    linear_solver="dense_finitediff",
    constraints=np.ones(transport_problem.n_states),
    abstol=1e-12,
    reltol=1e-8,
)
```

```python
# Define initial conditions
y0 = np.zeros((), dtype=transport_problem.state_dtype)
y0["z"][:] = 0.0
t0 = 0
tvals = np.arange(0, 100000, 100)
# Call the solver
solver.set_params_dict(
    {
        "transport": transport_param_values,
        "reaction": reaction_param_values,
    }
)
output = solver.make_output_buffers(tvals)
user_data = transport_problem.make_user_data()
solver.solve(t0=t0, tvals=tvals, y0=y0, y_out=output)

# Convert outputs to xarray dataset
ds = solver.as_xarray(tvals, output)
```

```python
# Plot the solution
ds.solution_z.isel(time=slice(None, None, 200)).plot.line(x="x", hue="time", col="chemical");
```

```python
# Plot the break-through curve
ds.solution_z.isel(x=-1).plot.line(x="time", col="chemical");
```

```python
ds.solution_z.isel(time=slice(None, None, 5)).plot(x="x", vmin=0, col="chemical")
```

```python

```
