---
jupyter:
  jupytext:
    formats: ipynb,md
    notebook_metadata_filter: -kernelspec
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.1
---

# Sinusoidal velocity function and boundary conditions
- sinusoidal velocity function,
- sinusoidal concentrations at the boundaries
- 1-dimensonal state variables

```python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sunode
import xarray as xr

import adrpy
import adrpy.velocity_funcs as velo
```

```python
# Define the reactions
def reaction(t, y, p):
    return {
        "y": -p.a * y.y,  # first order decay
    }
```

```python
# Define parameter values
transport_param_dims = {
    "dx": (),  # length of each cell
    "length": (),  # length of the total domain
    "transported": {"y": (), "z": ()},  # indicate if the state is subject to transport
    "v": {
        "amplitude": (),
        "frequency": (),
        "phase_shift": (),
        "mean_val": (),
    },
    "dispersivity": (),
    "molecular_diffusion": (),
    "porosity": (),
    "left": {
        "inflow_concentration": {
            "amplitude": {"y": ()},
            "frequency": {"y": ()},
            "phase_shift": {"y": ()},
            "mean_val": {"y": ()},
        }
    },
    "right": {
        "inflow_concentration": {
            "amplitude": {"y": ()},
            "frequency": {"y": ()},
            "phase_shift": {"y": ()},
            "mean_val": {"y": ()},
        },
    },
}
transport_param_values = {
    "dx": 0.01,
    "length": 2.0,
    "transported": {"y": 1.0, "z": 1.0},
    "v": {
        "amplitude": 2e-5,
        "frequency": 1 / 86400,
        "phase_shift": 0.0,
        "mean_val": -5e-6,
    },
    "dispersivity": 1e-2,
    "molecular_diffusion": 1e-9,
    "porosity": 0.3,
    "left": {
        "inflow_concentration": {
            "amplitude": {"y": 0.5},
            "frequency": {"y": 8 / 86400},
            "phase_shift": {"y": 0.0},
            "mean_val": {"y": 1.0},
        }
    },
    "right": {
        "inflow_concentration": {
            "amplitude": {"y": 0.5},
            "frequency": {"y": 5 / 86400},
            "phase_shift": {"y": 0.0},
            "mean_val": {"y": 1.0},
        },
    },
}
reaction_param_values = {
    "a": 1e-5,
    "c_norm": {"y": 1.0, "z": 1.0},
    "spatial_coordinate": -333.0,
}
```

```python
coords = {
    "x": np.arange(
        transport_param_values["dx"] / 2,
        transport_param_values["length"],
        transport_param_values["dx"],
    )
}
```

```python
# Set up the reaction problem
state_shapes = {"y": ()}
reaction_param_dims = {"a": (), "c_norm": {"y": ()}, "spatial_coordinate": ()}
reaction_problem = sunode.SympyProblem(
    params=reaction_param_dims,
    states=state_shapes,
    rhs_sympy=reaction,
    derivative_params=(),
)
```

```python
# Set up the transport problem
transport_problem = adrpy.TransportProblem(
    reaction_problem=reaction_problem,
    left_boundary="sinusoidal",
    right_boundary="sinusoidal",
    velocity_func=velo.sinusoidal_velocity,
    transport_param_dims=transport_param_dims,
    coords=coords,
)
```

```python
# Generate the solver
solver = sunode.solver.Solver(
    transport_problem,
    solver="BDF",
    linear_solver="dense_finitediff",
    constraints=np.ones(transport_problem.n_states),
    abstol=1e-12,
    reltol=1e-8,
)
```

```python
# Define initial conditions
y0 = np.zeros((), dtype=transport_problem.state_dtype)
y0["y"][:] = 0.0
t0 = 0
tvals = np.arange(0, 100000, 100)
# Call the solver
solver.set_params_dict(
    {
        "transport": transport_param_values,
        "reaction": reaction_param_values,
    }
)
output = solver.make_output_buffers(tvals)
user_data = transport_problem.make_user_data()
solver.solve(t0=t0, tvals=tvals, y0=y0, y_out=output)

# Convert outputs to xarray dataset
ds = solver.as_xarray(tvals, output)
```

```python
# Plot the solution
ds.solution_y.isel(time=slice(None, None, 200)).plot.line(x="x", hue="time");
```

```python
ds.solution_y.isel(time=slice(None, None, 5)).plot(x="x", vmin=0)
```
