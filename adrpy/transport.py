import sunode
import numpy as np
import sympy as sym
import numba
import nesttool


def dtype_dict_to_dtype(dtype_dict):
    dtype_tuples = []
    for key, item in dtype_dict.items():
        if isinstance(item, dict):
            dtype_tuples.append((key, dtype_dict_to_dtype(item)))
        elif isinstance(item, tuple):
            dtype_tuples.append((key, *item))
        else:
            assert False
    return np.dtype(dtype_tuples)


@numba.njit
def sine_wave(t, A, f, phi, m):
    """Sinusoidal function.

    Parameters
    ==========
    t: float
        time
    A: float or numpy array
        amplitude
    f: float or numpy array
        frequency
    phi: float or numpy array
        phase shift in radians
    m: float or numpy array
        mean value of the curve
    """
    return A * np.sin(2 * np.pi * f * t + phi) + m


class TransportProblem(sunode.problem.Problem):
    """
    A class to define a 1-D advection-dispersion transport problem in groundwater
    """
    def _check_boundary_params(self, transport_params):
        boundary_params = self.get_inflow_concentration_keys("left")
        boundary_params += self.get_inflow_concentration_keys("right")
        for keys in boundary_params:
            try:
                param = nesttool.getitem_nested(transport_params, keys)
            except KeyError:
                raise ValueError(
                    f"Parameter with keys {keys} is required in transport_params with "
                    "the current boundary conditions but it was not provided."
                )
            else:
                if not isinstance(param, dict):
                    raise ValueError(
                        f"Parameter given by keys {keys} should be a dictionary with "
                        f"keys {self.state_dtype.names} specifying its value for each state "
                        f"but it is {param}"
                    )
                else:
                    if not all(name in param for name in self._reaction_problem.state_dtype.names):
                        raise ValueError("Parameter must be given for all states.")

    def _check_params(self, transport_params):
        self._check_boundary_params(transport_params)

    def _init_params(self, reaction_problem, coords, transport_param_dims):
        coords = {**reaction_problem.coords, **coords}

        transport_params_subset = sunode.dtypesubset.DTypeSubset(
            transport_param_dims, [], fixed_dtype=sunode.basic.data_dtype, coords=coords
        )
        params = {
            "transport": transport_params_subset.dims,
            "reaction": reaction_problem.params_subset.dims,
        }
        self.params_subset = sunode.dtypesubset.DTypeSubset(
            params, [], coords=coords
        )

        self.coords = self.params_subset.coords
        self.params_dtype = self.params_subset.dtype

    def _init_states(self, reaction_problem):
        states = reaction_problem.state_subset.dims

        if self._spatial_dim_position == "last":
            def add_dim(val):
                return val[1] + [self.spatial_coordinate_name]

            states = nesttool.apply_func_nested(states, add_dim)

            self.state_subset = sunode.dtypesubset.DTypeSubset(
                states, [], fixed_dtype=sunode.basic.data_dtype, coords=self.coords
            )
            self.state_dtype = self.state_subset.dtype
        elif self._spatial_dim_position == "first":
            self.state_subset = sunode.dtypesubset.DTypeSubset(
                {"concentration": (self.spatial_coordinate_name, self.n_reaction_states)},
                [],
                fixed_dtype=sunode.basic.data_dtype, coords=self.coords
            )
            self.state_dtype = self.state_subset.dtype
        else:
            assert False

    def _init_boundaries(self, left_boundary, right_boundary):
        self.left_boundary = left_boundary
        self.right_boundary = right_boundary

    def __init__(
        self,
        reaction_problem,
        left_boundary,
        velocity_func,
        transport_param_dims,
        coords,
        spatial_coordinate_name="x",
        right_boundary=None,
        spatial_dim_position="last",
    ):
        """
        Parameters
        ----------
        reaction_problem : sunode.symode.problem.SympyProblem
            SympyProblem created with sunode that defines the reactions.
            The reaction parameters of the reaction problem are expected to contain at
            least the two keys "c_norm" and "spatial_coordinate".
            - "c_norm" is a dict having the same structure as the states and contains
              scaling factors for the states. It is used to scale all states to the
              same order of magnitude. Note that the scaling needs to also be
              implemented in the reaction problem to be consistent.
            - "spatial_coordinate" can be used to access the values of the spatial
              coordinate in the reaction problem (e.g. to define spatialy varying
              parameters). The value of this parameter can be any float as it will not
              be used. Rather, the corresponding values of the spatial coordinate (usually "x") will be
              written to it and can be accessed in the definition of the reactions.
        left_boundary: {"constant_concentration", "sinusoidal", "rectangular_pulse"}
            String indicating how the constant concentration boundary condition on the
            left boundary will be defined.
        velocity_func : callable
            Computes the velocity at time `t`. The calling signature is
            velocity_func(t, transport_params). It can use any parameters defined in
            transport_params – make sure to define `transport_param_dims` and the
            dictionary containing the parameter values accordingly.
        transport_param_dims : dict
            Nested dict specifying the dimensions of transport parameters. It must
            contain the following keys:
            - "dx": constant spatial discretization
            - "length": total domain length
            - "transported": dict with the state names as keys, the values are float
              (1.0 or 0.0) indicating if the state is subject to transport or not.
            - "dispersivity": longitudinal dispersivity
            - "molecular_diffusion": molecular diffusion coefficient
            - "left" (dict): parameters for the left boundary conditions. The required
              structure depends on the value of `left_boundary`:
              - "constant_concentration": `{"left": {"inflow_concentration": values}}`
              - "rectangular_pulse": `{
                                          "left": {
                                              "inflow_concentration": {
                                                  "background": background_values,
                                                  "pulse": pulse_values,
                                                }
                                           }
                                      }`
              - "sinusoidal": `{
                                   "left": {
                                       "inflow_concentration": {
                                           "amplitude": amplitude_values,
                                           "frequency": frequency_values,
                                           "phase_shift": phase_shift_values,
                                           "mean_val": mean_values,
                                       }
                                   }
                               }`
              The values of the parameters are again dictionaries with the structure
              of the reaction states, indicating the parameters for each state
              individually.
            - "right" (dict): parameters for the right boundary conditions. The required
              keys depend on the value of `right_boundary`.
        coords : dict
            Contains the coordinate values for the transport problem. It must contain
            the values indicate the spatial coordinate of the cells under the key `spatial_coordinate_name`.
        spatial_coordinate_name : str
            Name of the spatial coordinate, defualts to "x".
        right_boundary : str, optional
            String indicating how the constant concentration boundary condition on the
            right boundary will be defined. The same options my be used as for
            `left_boundary`. The right boundary conditions only needs to be defined if
            negative advective velocities occur.
        spatial_dim_position: ["first", "last"]
            This argument determines the structure of the right-hand-side matrix.
            When it is set to "last", the spatial dimension is inserted as the last
            dimension of the states. This means that the right-hand-side matrix first
            contains all entries relating to the first state at all points in space.
            When the argument is set to "first", the spatial dimension will be inserted
            as the first dimension. The right-hand-side matrix will then first contain
            entries for all states at the first spatial point, then entries for all
            states at the second point, and so on. For the 1-D reactive transport
            problem, this leads to a banded matrix where entries are restricted to the
            first few diagonals from the main diagonal (n_reaction_states diagonals in
            each direction). This can speed up the solution and enables the use of a
            linear solver for banded matrices (which leads to a further speed-up).
            When "first" is chosen, the reaction states will internally be represented
            as a flat instead of a nested array.
        """
        if spatial_dim_position not in ["last", "first"]:
            raise ValueError("Unknown spatial_dim_position")
        self._spatial_dim_position = spatial_dim_position
        self._reaction_problem = reaction_problem
        self._init_boundaries(left_boundary, right_boundary)
        self.velocity_func = velocity_func
        self.spatial_coordinate_name = spatial_coordinate_name
        self._init_params(reaction_problem, coords, transport_param_dims)
        self._init_states(reaction_problem)
        self._check_params(transport_param_dims)

        dtypes = [
            ("transport_params", self.params_subset.dtype["transport"]),
            ("reaction_user_data", self._reaction_problem.user_data_dtype),
            ("flat_transport_params", self.make_flat_transport_params_dtype()),
            (
                "tmp_nstates_nstates",
                np.float64,
                (self.n_states, self.n_states),
            ),
            (
                "tmp_nparams_nstates",
                np.float64,
                (self.n_params, self.n_states),
            ),
            (
                "tmp2_nparams_nstates",
                np.float64,
                (self.n_params, self.n_states),
            ),
            ("error_states", self.state_dtype),
            ("error_rhs", np.float64, (self.n_states,)),
            ("error_jac", np.float64, (self.n_states, self.n_states)),
            ("spatial_coordinate", np.float64, self.coords[self.spatial_coordinate_name].shape),
        ]
        self.user_data_dtype = np.dtype(dtypes)

    @property
    def n_reaction_states(self):
        return self._reaction_problem.n_states

    @property
    def n_cells(self):
        return len(self.coords[self.spatial_coordinate_name])

    def flat_solution_as_dict(self, outputs_flat):
        if self._spatial_dim_position == "last":
            return super().flat_solution_as_dict(outputs_flat)
        elif self._spatial_dim_position == "first":
            slices = self._reaction_problem.state_subset.flat_slices
            shapes = self._reaction_problem.state_subset.flat_shapes
            n_time = -1
            n_cells = self.n_cells
            n_reaction_states = self.n_reaction_states
            solution = outputs_flat.reshape((n_time, n_cells, n_reaction_states)).transpose([0, 2, 1])
            flat_views = {}
            for path in self._reaction_problem.state_subset.paths:
                shape = (n_time, *shapes[path], n_cells)
                flat_views[path] = solution[:, slices[path], :].reshape(shape)
            return sunode.dtypesubset.as_nested(flat_views)
        else:
            assert False

    def get_inflow_concentration_keys(self, boundary_side):
        if boundary_side == "left":
            boundary_type = self.left_boundary
        elif boundary_side == "right":
            boundary_type = self.right_boundary
        else:
            assert False
        if boundary_type == "sinusoidal":
            return [
                (boundary_side, "inflow_concentration", "amplitude"),
                (boundary_side, "inflow_concentration", "frequency"),
                (boundary_side, "inflow_concentration", "phase_shift"),
                (boundary_side, "inflow_concentration", "mean_val"),
            ]
        elif boundary_type == "constant_concentration":
            return [(boundary_side, "inflow_concentration")]
        elif boundary_type == "rectangular_pulse":
            return [
                (boundary_side, "inflow_concentration", "background"),
                (boundary_side, "inflow_concentration", "pulse"),
                (boundary_side, "inflow_concentration", "t_start"),
                (boundary_side, "inflow_concentration", "t_stop"),
            ]
        elif boundary_type == "xy":
            return []
        elif boundary_type is None:
            return []
        else:
            assert False

    @property
    def flat_transport_params(self):
        keys = [("transported",), ("c_norm",)]
        keys += self.get_inflow_concentration_keys("left")
        keys += self.get_inflow_concentration_keys("right")
        return keys

    def make_flat_transport_params_dtype(self):
        flat_dtypes_dict = {
            keys: (np.float64, (self.n_reaction_states,))
            for keys in self.flat_transport_params
        }
        dtypes_dict = sunode.dtypesubset.as_nested(flat_dtypes_dict)
        return dtype_dict_to_dtype(dtypes_dict)

    def make_inflow_concentration_func(self, boundary_type, side):
        """Define function for the inflow boundary condition."""
        n_reaction_states = self.n_reaction_states
        if boundary_type == "xy":

            @numba.njit
            def inflow_concentration(t, params, flat_transport_params):
                return np.ones(n_reaction_states)

        elif boundary_type == "constant_concentration":

            @numba.njit
            def inflow_concentration(t, params, flat_transport_params):
                c_norm = flat_transport_params.c_norm
                c_in = flat_transport_params[side].inflow_concentration
                x = np.empty_like(c_in)
                x[:] = c_in / c_norm
                return x

        elif boundary_type == "rectangular_pulse":

            @numba.njit
            def inflow_concentration(t, params, flat_transport_params):
                c_norm = flat_transport_params.c_norm
                c_in = flat_transport_params[side].inflow_concentration
                t_start = flat_transport_params[side].inflow_concentration.t_start
                t_stop = flat_transport_params[side].inflow_concentration.t_stop
                during_pulse = np.logical_and(t >= t_start,  t <= t_stop)
                c_inflow = c_in.background / c_norm
                c_inflow[during_pulse] = (c_in.pulse / c_norm)[during_pulse]
                return c_inflow

        elif boundary_type == "sinusoidal":

            @numba.njit
            def inflow_concentration(t, params, flat_transport_params):
                c_norm = flat_transport_params.c_norm
                c_in = flat_transport_params[side].inflow_concentration
                A = c_in.amplitude
                m = c_in.mean_val
                f = c_in.frequency
                phi = c_in.phase_shift
                return sine_wave(t, A, f, phi, m) / c_norm

        elif boundary_type is None:

            @numba.njit
            def inflow_concentration(t, params, flat_transport_params):
                return np.nan * np.ones(n_reaction_states)

        else:
            raise ValueError(f"Invalid boundary type: {boundary}")
        return inflow_concentration

    def make_dispersion_coefficient(self):
        @numba.njit
        def dispersion_coefficient(velocity, params):
            D = (
                params.dispersivity * np.abs(velocity)
                + params.porosity * params.molecular_diffusion
            )
            return D

        return dispersion_coefficient

    def make_rhs(self, debug=False):  # type: ignore
        reaction = sunode.symode.lambdify.lambdify_consts(
            "_rhs_reaction",
            argnames=["time", "state", "params"],
            expr=self._reaction_problem._simplify(
                np.array(self._reaction_problem._sym_dydt.T)
            ),
            varmap=self._reaction_problem._varmap,
            debug=debug,
        )

        n_reaction_states = self.n_reaction_states
        reaction_state_dtype = self._reaction_problem.state_dtype
        n_cells = self.n_cells
        velocity = self.velocity_func
        dispersion_coefficient = self.make_dispersion_coefficient()
        left_inflow_concentration = self.make_inflow_concentration_func(
            self.left_boundary, "left"
        )
        right_inflow_concentration = self.make_inflow_concentration_func(
            self.right_boundary, "right"
        )

        if self._spatial_dim_position == "last":

            @numba.njit
            def rhs_transport(out, t, y_flat, user_data):  # type: ignore
                params = user_data.transport_params
                reaction_user_data = user_data.reaction_user_data
                out[:] = 0

                reaction_state_flat = np.zeros(n_reaction_states)
                reaction_state = reaction_state_flat.view(reaction_state_dtype)[0]
                for i in range(n_cells):
                    x = user_data.spatial_coordinate[i]
                    reaction_user_data.params["spatial_coordinate"] = x
                    reaction_state_flat[:] = y_flat[i::n_cells]
                    reaction(
                        out[i::n_cells],
                        t,
                        reaction_state,
                        reaction_user_data.params,
                    )

                y = y_flat.reshape((n_reaction_states, n_cells))
                out = out.reshape((n_reaction_states, n_cells))
                assert y.shape == out.shape
                advection(out, t, y, user_data)
                dispersion(out, t, y, user_data)
                return 0
        elif self._spatial_dim_position == "first":
            @numba.njit
            def rhs_transport(out, t, y_flat, user_data):  # type: ignore
                params = user_data.transport_params
                reaction_user_data = user_data.reaction_user_data
                out[:] = 0

                reaction_state_flat = np.zeros(n_reaction_states)
                reaction_state = reaction_state_flat.view(reaction_state_dtype)[0]
                for i in range(n_cells):
                    x = user_data.spatial_coordinate[i]
                    reaction_user_data.params["spatial_coordinate"] = x
                    reaction_state_flat[:] = y_flat[n_reaction_states * i:n_reaction_states * (i + 1)]
                    reaction(
                        out[n_reaction_states * i:n_reaction_states * (i + 1)],
                        t,
                        reaction_state,
                        reaction_user_data.params,
                    )

                y = y_flat.reshape((n_cells, n_reaction_states))
                out = out.reshape((n_cells, n_reaction_states))
                assert y.shape == out.shape
                advection(out.T, t, y.T, user_data)
                dispersion(out.T, t, y.T, user_data)
                return 0
        else:
            assert False


        @numba.njit
        def advection(out, t, y, user_data):
            """1-D finite volume scheme for advection with upstream weighting."""
            # n_states, n_cells = y.shape
            params = user_data.transport_params
            transported = user_data.flat_transport_params.transported
            v = velocity(t, params)

            assert y.shape == out.shape

            if v >= 0:
                inflow = left_inflow_concentration(
                    t, params, user_data.flat_transport_params
                )
            else:
                inflow = right_inflow_concentration(
                    t, params, user_data.flat_transport_params
                )
            assert inflow.shape == (n_reaction_states,)

            v_abs = np.abs(v)

            for reactant in range(n_reaction_states):
                if transported[reactant] == 0:
                    continue

                if v >= 0:
                    y_out = y[reactant, 0]
                    y_in = inflow[reactant]
                    out[reactant, 0] += v_abs * (y_in - y_out) / params.dx

                    for i in range(1, n_cells):
                        y_in = y[reactant, i - 1]
                        y_out = y[reactant, i]
                        out[reactant, i] += v_abs * (y_in - y_out) / params.dx
                else:
                    y_out = y[reactant, n_cells - 1]
                    y_in = inflow[reactant]
                    out[reactant, -1] += v_abs * (y_in - y_out) / params.dx

                    for i in range(n_cells - 1):
                        y_in = y[reactant, i + 1]
                        y_out = y[reactant, i]
                        out[reactant, i] += v_abs * (y_in - y_out) / params.dx

        @numba.njit
        def dispersion(out, t, y, user_data):
            """1-D finite volume scheme for dispersion."""
            # n_states, n_cells = y.shape
            params = user_data.transport_params
            transported = user_data.flat_transport_params.transported
            v = velocity(t, params)
            D = dispersion_coefficient(v, params)

            assert y.shape == out.shape

            if v >= 0:
                inflow = left_inflow_concentration(
                    t, params, user_data.flat_transport_params
                )
            else:
                inflow = right_inflow_concentration(
                    t, params, user_data.flat_transport_params
                )

            assert inflow.shape == (n_reaction_states,)

            v_abs = np.abs(v)

            for reactant in range(n_reaction_states):
                if transported[reactant] == 0:
                    continue

                for i in range(1, n_cells - 1):
                    diff_in = y[reactant, i] - y[reactant, i - 1]
                    diff_out = y[reactant, i + 1] - y[reactant, i]
                    out[reactant, i] += (
                        D * (diff_out - diff_in) / params.dx ** 2
                    )

                if v >= 0:
                    # i == 0
                    diff_in = (y[reactant, 0] - inflow[reactant]) * 2
                    diff_out = y[reactant, 1] - y[reactant, 0]
                    out[reactant, 0] += (
                        D * (diff_out - diff_in) / params.dx ** 2
                    )

                    # i == -1
                    diff_in = y[reactant, -1] - y[reactant, -2]
                    diff_out = 0.
                    out[reactant, -1] += (
                        D * (diff_out - diff_in) / params.dx ** 2
                    )
                else:
                    # i == 0
                    diff_in = y[reactant, 0] - y[reactant, 1]
                    diff_out = 0.
                    out[reactant, 0] += (
                        D * (diff_out - diff_in) / params.dx ** 2
                    )

                    # i == -1
                    diff_in = (y[reactant, -1] - inflow[reactant]) * 2
                    diff_out = y[reactant, -2] - y[reactant, -1]
                    out[reactant, -1] += (
                        D * (diff_out - diff_in) / params.dx ** 2
                    )

        return rhs_transport

    def make_sundials_rhs(self):
        from sunode.basic import ffi, lib

        rhs = self.make_rhs()

        N_VGetArrayPointer_Serial = lib.N_VGetArrayPointer_Serial
        N_VGetLength_Serial = lib.N_VGetLength_Serial

        state_dtype = self.state_dtype
        user_dtype = self.user_data_dtype
        user_ndtype = numba.from_dtype(user_dtype)
        user_ndtype_p = numba.types.CPointer(user_ndtype)
        func_type = numba.core.typing.cffi_utils.map_type(
            ffi.typeof("CVRhsFn")
        )
        func_type = func_type.return_type(
            *(func_type.args[:-1] + (user_ndtype_p,))
        )

        n_states = self.n_states

        @numba.cfunc(func_type)
        def rhs_wrapper(t, y_, out_, user_data_):  # type: ignore
            y_ptr = N_VGetArrayPointer_Serial(y_)
            n_vars = N_VGetLength_Serial(y_)

            if n_vars != n_states:
                return -1

            if N_VGetLength_Serial(out_) != n_states:
                return -1
            if N_VGetLength_Serial(y_) != n_states:
                return -1

            out_ptr = N_VGetArrayPointer_Serial(out_)
            y = numba.carray(y_ptr, (n_vars,))  # .view(state_dtype)[0]
            out = numba.carray(out_ptr, (n_vars,))

            user_data = numba.carray(user_data_, (1,), user_dtype)[0]

            return rhs(out, t, y, user_data)

        return rhs_wrapper

    def update_params(self, user_data: np.ndarray, params: np.ndarray) -> None:
        user_data.transport_params.fill(params["transport"])
        user_data.reaction_user_data.params.fill(params["reaction"])
        user_data.spatial_coordinate[:] = self.coords[self.spatial_coordinate_name].values
        for keys in self.flat_transport_params:
            if keys == ("c_norm",):
                parent = params["reaction"]
            else:
                parent = params["transport"]
            param = nesttool.getitem_nested(parent, keys)
            param = self._reaction_problem.state_subset.from_dict(
                sunode.dtypesubset._as_dict(param)
            )
            user_data_param = nesttool.getattr_nested(
                user_data.flat_transport_params, keys
            )
            user_data_param[:] = param[None].view(np.float64)

    def update_subset_params(
        self, user_data: np.ndarray, params: np.ndarray
    ) -> None:
        raise NotImplementedError()

    def update_remaining_params(
        self, user_data: np.ndarray, params: np.ndarray
    ) -> None:
        raise NotImplementedError()

    def extract_params(self, user_data: np.ndarray, out=None) -> None:
        if out is None:
            out = np.full((1,), np.nan, dtype=self.params_dtype)[0]
        out["transport"].fill(user_data.transport_params)
        out["reaction"].fill(user_data.reaction_user_data.params)
        return out

    def make_rhs_jac_prod(self, debug=False):
        problem = self._reaction_problem
        expr = problem._sym_dydt_jac @ problem._sym_lamda
        reaction_matmult = sunode.symode.lambdify.lambdify_consts(
            "_jac",
            argnames=["time", "state", "lamda", "params"],
            expr=problem._simplify(expr),
            varmap=problem._varmap,
            debug=debug,
        )
        n_cells = self.n_cells
        reaction_state_dtype = self._reaction_problem.state_dtype
        velocity = self.velocity_func
        dispersion_coefficient = self.make_dispersion_coefficient()

        @numba.njit
        def reaction_jac_prod(out, time, state, lamda, params):
            for i in range(n_cells):
                reaction_matmult(
                    out[i::n_cells],
                    time=time,
                    state=state[i::n_cells].copy().view(reaction_state_dtype)[0],
                    lamda=lamda[i::n_cells],
                    params=params,
                )

        @numba.njit
        def add_transport_jac_prod(out, time, state, lamda, params, transported_flat):
            v = velocity(time, params)
            D = dispersion_coefficient(v, params)
            a = np.abs(v) / params.dx
            d = D / params.dx ** 2
            for i in range(n_cells):
                if v >= 0:
                    if i == 0:
                        out[i::n_cells] += transported_flat * (
                            -(2 * a + 3 * d) * lamda[i::n_cells]
                            + d * lamda[(i + 1) :: n_cells]
                        )
                    elif i == n_cells - 1:
                        out[i::n_cells] += transported_flat * (
                            (a + d) * lamda[(i - 1) :: n_cells]
                            - (a + d) * lamda[i::n_cells]
                        )
                    else:
                        out[i::n_cells] += transported_flat * (
                            (a + d) * lamda[(i - 1) :: n_cells]
                            - (a + 2 * d) * lamda[i::n_cells]
                            + d * lamda[(i + 1) :: n_cells]
                        )
                else:
                    if i == 0:
                        out[i::n_cells] += transported_flat * (
                            -(a + d) * lamda[i::n_cells]
                            + (a + d) * lamda[(i + 1) :: n_cells]
                        )
                    elif i == n_cells - 1:
                        out[i::n_cells] += transported_flat * (
                            d * lamda[(i - 1) :: n_cells]
                            - (2 * a + 3 * d) * lamda[i::n_cells]
                        )
                    else:
                        out[i::n_cells] += transported_flat * (
                            d * lamda[(i - 1) :: n_cells]
                            - (a + 2 * d) * lamda[i::n_cells]
                            + (a + d) * lamda[(i + 1) :: n_cells]
                        )

        @numba.njit
        def jac_prod(out, v, t, y, fy, user_data):
            reaction_jac_prod(out, t, y, v, user_data.reaction_user_data.params)
            add_transport_jac_prod(
                out,
                t,
                y,
                v,
                user_data.transport_params,
                user_data.flat_transport_params.transported,
            )
            return 0

        return jac_prod

    def make_sundials_jac_prod(self):  # type: ignore
        from sunode.basic import ffi, lib

        jac_prod = self.make_rhs_jac_prod()

        user_dtype = self.user_data_dtype
        state_dtype = self.state_dtype

        N_VGetArrayPointer = lib.N_VGetArrayPointer
        N_VGetLength = lib.N_VGetLength_Serial

        user_ndtype = numba.from_dtype(user_dtype)
        user_ndtype_p = numba.types.CPointer(user_ndtype)

        func_type = numba.core.typing.cffi_utils.map_type(
            ffi.typeof("CVLsJacTimesVecFn")
        )
        args = list(func_type.args)
        args[-2] = user_ndtype_p
        func_type = func_type.return_type(*args)

        @numba.cfunc(func_type)
        def jac_prod_wrapper(
            v_,
            out_,
            t,
            y_,
            fy_,
            user_data_,
            tmp_,
        ):  # type: ignore
            n_vars = N_VGetLength(v_)

            v_ptr = N_VGetArrayPointer(v_)
            out_ptr = N_VGetArrayPointer(out_)
            y_ptr = N_VGetArrayPointer(y_)
            fy_ptr = N_VGetArrayPointer(fy_)

            v = numba.carray(v_ptr, (n_vars,))
            y = numba.carray(y_ptr, (n_vars,))
            out = numba.carray(out_ptr, (n_vars,))
            fy = numba.carray(fy_ptr, (n_vars,))
            user_data = numba.carray(user_data_, (1,), user_dtype)[0]

            return jac_prod(out, v, t, y, fy, user_data)

        return jac_prod_wrapper
