from setuptools import setup

setup(
    name="adrpy",
    version="0.2.0",
    description="Define 1-D Advection-Dispersion-Reaction models with a finite volume discretization",
    url="https://gitlab.com/astoeriko/adrpy",
    author="Anna Störiko",
    author_email="anna.stoeriko@gmx.de",
    license="MIT",
    packages=["adrpy"],
    install_requires=[
        "numpy",
        "sympy",
        "numba",
        "sunode",
        "nesttool @ git+https://gitlab.com/astoeriko/nesttool.git#egg=nesttool-master",
    ],
)
